package cz.cvut.fit.szolloli.client.resources;

import cz.cvut.fit.szolloli.client.dto.CameraDTO;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.client.RootUriTemplateHandler;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class CameraResource {
    private final RestTemplate restTemplate;

    public CameraResource(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.rootUri(ROOT_RESOURCE_URL).build();
    }

    private final String COLLECTION_PAGE_URI = "/?page={page}&size={size}";
    private final String ROOT_RESOURCE_URL = "http://localhost:8080/cameras";
    private final String ID = "/{id}";

    public URI create(CameraDTO camera) {
        return restTemplate.postForLocation("/", camera);
    }

    public List<CameraDTO> readById(Integer id) {
        CameraDTO camera = restTemplate.getForObject(ROOT_RESOURCE_URL + ID, CameraDTO.class, id);


        return Arrays.asList(camera);
    }

    public List<CameraDTO> readAll(int page, int size) {
        ResponseEntity<PagedModel<CameraDTO>> result = restTemplate.exchange(COLLECTION_PAGE_URI,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<PagedModel<CameraDTO>>() {},
                page,
                size
        );

        System.out.println(result.getBody());

        return new ArrayList<>(result.getBody().getContent());
//        return result.getBody();
    }
}
