package cz.cvut.fit.szolloli.client.resources;

import cz.cvut.fit.szolloli.client.dto.CameraDTO;
import cz.cvut.fit.szolloli.client.dto.CustomerDTO;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomerResource {
    private final RestTemplate restTemplate;

    public CustomerResource(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.rootUri(ROOT_RESOURCE_URL).build();
    }

    private final String COLLECTION_PAGE_URI = "/?page={page}&size={size}";
    private final String ROOT_RESOURCE_URL = "http://localhost:8080/customers";
    private final String ID = "/{id}";

    public URI create(CustomerDTO camera) {
        return restTemplate.postForLocation("/", camera);
    }

    public List<CustomerDTO> readById(Integer id) {
        CustomerDTO camera = restTemplate.getForObject(ROOT_RESOURCE_URL + ID, CustomerDTO.class, id);


        return Arrays.asList(camera);
    }

    public PagedModel<CustomerDTO> readAll(int page, int size) {
        ResponseEntity<PagedModel<CustomerDTO>> result = restTemplate.exchange(COLLECTION_PAGE_URI,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<PagedModel<CustomerDTO>>() {},
                page,
                size
        );


        return result.getBody();
//        return new ArrayList<>(result.getBody().getContent());
//        return result.getBody();
    }
}
