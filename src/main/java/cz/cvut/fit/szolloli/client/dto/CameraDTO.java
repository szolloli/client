package cz.cvut.fit.szolloli.client.dto;

import cz.cvut.fit.szolloli.client.resources.CameraResource;
import org.springframework.hateoas.RepresentationModel;

public class CameraDTO extends RepresentationModel<CameraDTO> {
    private Integer id;
    private String  name;
    private Double  price;
    private Boolean  film;;

    public CameraDTO() {};

    public CameraDTO(Integer id, String name, Double price, Boolean film) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.film = film;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public Boolean getFilm() {
        return film;
    }
}
