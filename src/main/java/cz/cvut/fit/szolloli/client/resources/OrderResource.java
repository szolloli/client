package cz.cvut.fit.szolloli.client.resources;

import cz.cvut.fit.szolloli.client.dto.CameraDTO;
import cz.cvut.fit.szolloli.client.dto.CameraOrderDTO;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OrderResource {
    private final RestTemplate restTemplate;

    public OrderResource(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.rootUri(ROOT_RESOURCE_URL).build();
    }

    private final String COLLECTION_PAGE_URI = "/?page={page}&size={size}";
    private final String ROOT_RESOURCE_URL = "http://localhost:8080/orders";
    private final String ID = "/{id}";

    public URI create(CameraOrderDTO camera) {
        return restTemplate.postForLocation("/", camera);
    }

    public List<CameraOrderDTO> readById(Integer id) {
        CameraOrderDTO camera = restTemplate.getForObject(ROOT_RESOURCE_URL + ID, CameraOrderDTO.class, id);


        return Arrays.asList(camera);
    }

    public PagedModel<CameraOrderDTO> readAll(int page, int size) {
        ResponseEntity<PagedModel<CameraOrderDTO>> result = restTemplate.exchange(COLLECTION_PAGE_URI,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<PagedModel<CameraOrderDTO>>() {},
                page,
                size
        );

        return result.getBody();
    }
}
