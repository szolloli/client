package cz.cvut.fit.szolloli.client;

import cz.cvut.fit.szolloli.client.resources.CameraResource;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.HypermediaRestTemplateConfigurer;

import java.net.URL;
import java.util.ResourceBundle;

@SpringBootApplication
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class ClientApplication extends Application {

	@Bean
	RestTemplateCustomizer customizer(HypermediaRestTemplateConfigurer c) {
		return restTemplate -> {c.registerHypermediaTypes(restTemplate);};
	}

	@Override
	public void start(Stage primaryStage) throws Exception{
		System.out.println("Start");
		Parent root = FXMLLoader.load(getClass().getResource("/source.fxml"));
		System.out.println("start end");
		primaryStage.setTitle("Šemeštrálka");
		primaryStage.setScene(new Scene(root));










		TableView table = (TableView) root.lookup("table");
		TableColumn col = new TableColumn<>("hurhaj");
//		table.getColumns().addAll(col);
		primaryStage.show();

	}


	public static void main(String[] args) {
		launch(args);
	}

}
