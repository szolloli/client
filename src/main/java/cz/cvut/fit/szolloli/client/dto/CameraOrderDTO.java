package cz.cvut.fit.szolloli.client.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Date;
import java.util.List;

public class CameraOrderDTO extends RepresentationModel<CameraOrderDTO> {
    private Integer id;
    private Double totalPrice;
    private Date date;
    private List<Integer> cameraIds;
    private Integer customerId;

    public Integer getId() {
        return id;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public Date getDate() {
        return date;
    }

    public List<Integer> getCameraIds() {
        return cameraIds;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public CameraOrderDTO() {};

    public CameraOrderDTO(Integer id, Double totalPrice, Date date, List<Integer> cameraIds, Integer customerId) {
        this.id = id;
        this.totalPrice = totalPrice;
        this.date = date;
        this.cameraIds = cameraIds;
        this.customerId = customerId;
    }
}
