package cz.cvut.fit.szolloli.client;
import cz.cvut.fit.szolloli.client.dto.CameraDTO;
import cz.cvut.fit.szolloli.client.dto.CameraOrderDTO;
import cz.cvut.fit.szolloli.client.dto.CustomerDTO;
import cz.cvut.fit.szolloli.client.resources.CameraResource;
import cz.cvut.fit.szolloli.client.resources.CustomerResource;
import cz.cvut.fit.szolloli.client.resources.OrderResource;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.hateoas.PagedModel;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class Controller implements Initializable {
    private CameraResource cameraResource = new CameraResource(new RestTemplateBuilder());

    private CustomerResource customerResource = new CustomerResource(new RestTemplateBuilder());

    private OrderResource orderResource = new OrderResource(new RestTemplateBuilder());

    Integer specialNumber;




    @FXML
    private TableView myTable;



    @FXML
    private TextField textField;


    @FXML
    public void findById() {
        String id = textField.getText();
        if (id.isEmpty())
            return;

        System.out.println("findById");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {


    }


    @FXML
    public void findAllCameras() {
        specialNumber = 0;
        myTable.getColumns().clear();
        System.out.println("init");
        TableColumn id = new TableColumn("ID");
        TableColumn name = new TableColumn("NAME");
        TableColumn price = new TableColumn("PRICE");


        id.prefWidthProperty().bind(myTable.widthProperty().divide(3));
        name.prefWidthProperty().bind(myTable.widthProperty().divide(3));
        price.prefWidthProperty().bind(myTable.widthProperty().divide(3));

        myTable.getColumns().addAll(id, name, price);
        myTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);


        List<CameraDTO> cameras = cameraResource.readAll(0,10);
        CameraDTO[] array = new CameraDTO[cameras.size()];
        cameras.toArray(array);
        final ObservableList<cz.cvut.fit.szolloli.client.dto.CameraDTO> data = FXCollections.observableArrayList(
            array
        );

        id.setCellValueFactory(new PropertyValueFactory<cz.cvut.fit.szolloli.client.dto.CameraDTO, String>("id"));

        name.setCellValueFactory(new PropertyValueFactory<cz.cvut.fit.szolloli.client.dto.CameraDTO, String>("name"));

        price.setCellValueFactory(new PropertyValueFactory<cz.cvut.fit.szolloli.client.dto.CameraDTO, String>("price"));

        myTable.setItems(data);

        System.out.println("init end");
    }

    @FXML
    public void findAllCustomers() {
        specialNumber = 1;
        myTable.getColumns().clear();
        System.out.println("init");
        TableColumn id = new TableColumn("ID");
        TableColumn name = new TableColumn("NAME");
        TableColumn address = new TableColumn("ADDRESS");
        TableColumn phoneNumber = new TableColumn("PHONE NUMBER");

        id.prefWidthProperty().bind(myTable.widthProperty().divide(4));
        name.prefWidthProperty().bind(myTable.widthProperty().divide(4));
        address.prefWidthProperty().bind(myTable.widthProperty().divide(4));
        phoneNumber.prefWidthProperty().bind(myTable.widthProperty().divide(4));


        myTable.getColumns().addAll(id, name, address, phoneNumber);
        myTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);


        PagedModel<CustomerDTO> page = customerResource.readAll(0,10);
        CustomerDTO[] array = new CustomerDTO[(int) page.getMetadata().getTotalElements()];
        int i = 0;
        for (CustomerDTO customer : page) {
            array[i] = customer;
            i++;
        }
        final ObservableList<cz.cvut.fit.szolloli.client.dto.CustomerDTO> data = FXCollections.observableArrayList(
              array
        );

        id.setCellValueFactory(new PropertyValueFactory<CustomerDTO, String>("id"));

        name.setCellValueFactory(new PropertyValueFactory<CustomerDTO, String>("name"));

        address.setCellValueFactory(new PropertyValueFactory<CustomerDTO, String>("address"));

        phoneNumber.setCellValueFactory(new PropertyValueFactory<CustomerDTO, String>("phoneNumber"));

        myTable.setItems(data);

        System.out.println("init end");
    }


    @FXML
    public void findAllOrders() {
        specialNumber = 2;
        myTable.getColumns().clear();
        System.out.println("init");
        TableColumn id = new TableColumn("ID");
        TableColumn date = new TableColumn("DATE");
        TableColumn totalPrice = new TableColumn("TOTAL PRICE");
        TableColumn customerId = new TableColumn("CUSTOMER ID");
        TableColumn cameraIds = new TableColumn("CAMERA IDS");

        id.prefWidthProperty().bind(myTable.widthProperty().divide(5));
        date.prefWidthProperty().bind(myTable.widthProperty().divide(5));
        totalPrice.prefWidthProperty().bind(myTable.widthProperty().divide(5));
        customerId.prefWidthProperty().bind(myTable.widthProperty().divide(5));
        cameraIds.prefWidthProperty().bind(myTable.widthProperty().divide(5));


        myTable.getColumns().addAll(id, date, totalPrice, customerId, cameraIds);
        myTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);


        PagedModel<CameraOrderDTO> page = orderResource.readAll(0,10);
        CameraOrderDTO[] array = new CameraOrderDTO[(int) page.getMetadata().getTotalElements()];
        int i = 0;
        for (CameraOrderDTO order : page) {
            array[i] = order;
            System.out.println(order.getId());
            i++;
        }
        final ObservableList<CameraOrderDTO> data = FXCollections.observableArrayList(
              array
        );

        id.setCellValueFactory(new PropertyValueFactory<CameraOrderDTO, String>("id"));

        date.setCellValueFactory(new PropertyValueFactory<CameraOrderDTO, String>("date"));

        totalPrice.setCellValueFactory(new PropertyValueFactory<CameraOrderDTO, String>("totalPrice"));

        customerId.setCellValueFactory(new PropertyValueFactory<CameraOrderDTO, String>("customerId"));

        cameraIds.setCellValueFactory(new PropertyValueFactory<CameraOrderDTO, String>("cameraIds"));

        myTable.setItems(data);

        System.out.println("init end");
    }

}
