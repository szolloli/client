package cz.cvut.fit.szolloli.client.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class CustomerDTO extends RepresentationModel<CustomerDTO> {
    private Integer id;
    private String name;
    private String address;
    private String phoneNumber;
    private List<Integer> orderIds;

    public CustomerDTO(){};

    public CustomerDTO(Integer id, String name, String address, String phoneNumber, List<Integer> orderIds) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.orderIds = orderIds;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public List<Integer> getOrderIds() {
        return orderIds;
    }
}
